
# Coding Challenge


## The exercise

The React Native application is able to load data from a server and playback the associated audio content.

### The application

1. Fetch the content of the manifest file<sup>1</sup> and show it in a scrollable list. (support reloading the manifest file).
   - Each tile shows a non interactive rating element displayed as stars
   - Each tile shows an interactive element to set the song as a favorite. There can only be one favorite set. As soon as a song is set as favorite, all others are set to non-favorite 
2. Upon clicking on one of the list entries, the application shall navigate to a new screen
3. This new screen displays the content of the selected element. Additionally it shows:
   - A play button on top of the cover image which starts / stops the playback of the associated audio file
   - Audio playback control (load, play, pause and seek)
   - A position slider which updates its position depending on the position of the currently playing audio. It should also be able to control the current audio position with this slider<sup>2</sup>
   - The current play time and the audio duration<sup>2</sup> 
   - The interactive rating element displayed as stars. Rating can be set here
   - The interactive favorite element to set the song as a favorite. (As above, there can only be one favorite set. As soon as a song is set as favorite, all others are set to non-favorite) 

## The following rules apply to all coding challenges:

- Supply a clean and readable unit testing suite
- Supply clean code (e.g. SOLID principles, etc)
- Supply a scaleable, clean architecture
- Supply build / make / run instructions if required
- Preferably, provide the code in a Git repository
- You can find some assets in the `assets` folder.

### Footnotes

<sup>1</sup>You can find the [manifest file here](data/manifest.json). [Direct link](https://gitlab.com/dono-dev/react-native-code-challenge/-/raw/main/data/manifest.json)

<sup>2</sup>Update the value at an appropriate frequency

### User Interface Mockup

![enter image description here](https://gitlab.com/dono-dev/react-native-code-challenge/-/raw/main/mockup.png)